public class WaffleMaker
{
	public int averageCookTimeMinutes;
	public String patternDesign;
	public int temperatureFahrenheit;
	
	public void finishedCooking()
	{
		System.out.println("its been " + averageCookTimeMinutes + " minutes, your waffle is finished!");
		
	}
	
	public void patternTypes()
	{
		System.out.println("Your waffle has " + patternDesign + " patterns!");
	}
}