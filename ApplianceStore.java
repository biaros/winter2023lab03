import java.util.Scanner;
public class ApplianceStore
{
	public static void main(String[] args)
	{
		Scanner scan = new Scanner(System.in);
		WaffleMaker[] waffleMakers = new WaffleMaker[4];
		
		for (int count = 0; count < waffleMakers.length; count++)
		{
			waffleMakers[count] = new WaffleMaker();
			
			System.out.println("Average cook time of the waffle maker in minutes?");
			waffleMakers[count].averageCookTimeMinutes = Integer.parseInt(scan.nextLine());
			
			System.out.println("Temperature of the waffle maker, in Fahrenheit?");
			waffleMakers[count].temperatureFahrenheit = Integer.parseInt(scan.nextLine());
			
			System.out.println("Pattern design of the waffle maker?");
			waffleMakers[count].patternDesign = scan.nextLine();
		}
		
		System.out.println("Average cook time of the last waffle maker: " + waffleMakers[(waffleMakers.length - 1)].averageCookTimeMinutes + " minutes");
		System.out.println("Temperature of the last waffle maker: " +  waffleMakers[(waffleMakers.length - 1)].temperatureFahrenheit + "F");
		System.out.println("Pattern design of the last waffle maker: " +  waffleMakers[(waffleMakers.length - 1)].patternDesign);
		
		waffleMakers[0].finishedCooking();
		waffleMakers[0].patternTypes();
	}
}